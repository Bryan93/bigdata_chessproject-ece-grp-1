import requests
import json
import ndjson



r = requests.get('https://lichess.org/api/tournament')

data = r.json()


finished_tournament = data['finished']

with open('data.json', 'w') as f:
    json.dump(finished_tournament, f, indent=2)


listIdTournament = []

for tournament in finished_tournament:
    listIdTournament.append(tournament['id'])



param = {'id': 'SqhDI3TC'}

r1 = requests.get('https://lichess.org/api/tournament/{id}/games'.format(**param), headers={'Accept': 'application/x-ndjson'}, params={'opening': 'true'})
data_games = r1.json(cls=ndjson.Decoder)

listIdGames = []



for games in data_games:
    games.pop('variant', None)
    games.pop('speed', None)
    games.pop('perf', None)
    games.pop('players', None)
    games.pop('tournament', None)
    games.pop('clock', None)

with open('data_games.json', 'w') as f:
    json.dump(data_games, f, indent=2)

print('Données récupérées')








