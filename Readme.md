# Projet Big Data - Statistiques jeux d'echec

Notre projet a pour but de créer une application basée sur des données de  jeux d’échecs .
Nous avons récupéré les données à partir d’un dataset sur le site Kaggle ,ils ont été collecté à l'aide de l' API Lichess, cette dernière permet de collecter l'historique de jeu des utilisateurs.
L’application devra afficher sur un Dashboard des graphes contenant les informations sur les parties des jeux d’échecs :( nombre de victoire ,type)


  
## 💻 Built with
- [Grafana] (https://grafana.com): Dashboard
- [PostgreSQL](https://www.postgresql.org/): Database
- [LichessAPI] (https://lichess.org/api): API permettant de récupérer les données des parties d'échecs Lichess

## 🙇 Contributors
<center><table>
<tr>
    <td>
      <a href="https://gitlab.com/cbeniddir">
        <p align="center">
          <img src="https://secure.gravatar.com/avatar/46abe54407fc604f72672d7685764c20?s=180&d=identicon" width=60>
        </p>
      </a>
    </td>
    <td>
      <a href="https://gitlab.com/Bryan93">
        <img src="https://secure.gravatar.com/avatar/87eeabd9db3cc4acf234d801fce82fc7?s=180&d=identicon" width=60>
      </a>
    </td>
    <td>
      <a href="https://gitlab.com/nadaH">
        <p align="center">
          <img src="https://secure.gravatar.com/avatar/b0d6973ba27b282af3de9bd41f7ad474?s=800&d=identicon" width=60>
        </p>
      </a>
    </td>
  </tr>
  <tr>
    <th>Beniddir Céline</th>
    <th>Jaliot Bryan</th>
    <th>Hannachi Nada</th>
  </tr>
</table></center>

## Project Clone

Cloner le projet en utilisant git Clone
##### `git clone https://gitlab.com/Bryan93/bigdata_chessproject-ece-grp-1.git`


## 🛠️ Installation Steps

### Commande d'execution 

#### Etape 1 - Build le docker image de l'application
docker-compose up --build

#### Etape 2 - Insertion des données dans la base 

**Se placer dans le dossier du repo GIT**

docker exec -i postgreschess psql -U postgres bigdatadb < postgres/backup.sql

#### Etape 3 - Accéder à grafana 

Accéder au serveur via `http://127.0.0.1:8000/`

#### Identifiant de connexion grafana

**user:** admin
**password:** admin

PS: Vous pouvez skip la connexion tout comme vous pouvez modifier le mot de passe, je vous préconise de le changer pour une utilisation ultérieure

## Rapport Projet

De nombreux travaux ont été faits dans ce projet avec notamment l'utilisation de Kubernetes et minikube. Malheureusement, nous avons connus de nombreux échecs. C'est pour cela que vous trouverez dans le repo, un mini rapport qui explique les technologies que nous avons utilisé et que nous avons voulu mettre en place.


